#!/bin/python

import re, sys

from tempfile import mkstemp
from shutil import move
from os import remove, close



def replace(file_path):
    #Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                if(re.search('.mw.lab.eng.bos.redhat.com', line)):
                  line = re.sub('(.*)\.mw\.lab\.eng\.bos\.redhat\.com', r'\1', line)
                new_file.write(line)
    close(fh)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)



#=========================#
#       main starts       #
#=========================#

duplicates_list = open('duplicates_list', 'r')
duplicates_list_tmp = duplicates_list
duplicates_list_str = duplicates_list_tmp.read()

counter = 0
with open('duplicates_list') as file:
  counter3 = 0
  for searched_item in file:
    searched_item = re.sub('(.*)\n', r'\1', searched_item)
    counter3 = counter3 + 1
    with open('duplicates_list') as file2:
      counter2 = 0
      for line_of_duplicate_list in file2:
        counter2 = counter2 + 1
        if(searched_item in line_of_duplicate_list and counter2 != counter3 and not(re.match(searched_item+'[0-9]-?.*$', line_of_duplicate_list))):
          if(re.search(r'\-', line_of_duplicate_list) and re.match(searched_item+"-.+?", line_of_duplicate_list)):
            counter = counter + 1
            print(str(counter)+". duplicate: line "+str(counter3)+" is: "+searched_item+"\n              line "+str(counter2)+" is: "+line_of_duplicate_list)
         
